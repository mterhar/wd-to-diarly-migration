import sys
import os
import shutil
import zipfile
import json
from datetime import datetime

run_at = datetime.now()
run_at_iso = run_at.strftime("%Y-%m-%d %H%M%S")

if os.environ['LASTMODIFIEDBY'] is not None:
    LASTMODIFIEDBY = os.environ['LASTMODIFIEDBY']
else:
    print('please set LASTMODIFIEDBY environment varialbe.')
    sys.exit()

if len(sys.argv) > 1 and sys.argv[1] == 'test':
    SOURCE_PATH = os.path.join('.', 'sample data', 'source')
else:
    SOURCE_PATH = os.path.join('.', 'data', 'source')

INTERMEDIATE_PATH = os.path.join('.', 'intermediate')
DESTINATION_PATH = os.path.join('.', 'data', 'destination', run_at_iso)
DESTINATION_PATH_ZIP = os.path.join(
    DESTINATION_PATH,
    'Diarly - Export (' + run_at.strftime("%Y-%m-%d") + ')'
)


def find_extract_zip(file_name):
    if file_name.lower().endswith('.zip'):
        path_to_zip_file = os.path.join(SOURCE_PATH, file_name)
        with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
            extract_target = os.path.join(INTERMEDIATE_PATH, file_name)
            os.makedirs(extract_target, exist_ok=True)
            zip_ref.extractall(extract_target)
            print("extracted %s to %s" % (file_name, extract_target))
            return INTERMEDIATE_PATH


def validate_wd(extracted_journal):
    entries, photos, journals = (False, False, False)
    extracted_journal_dirs = os.listdir(extracted_journal)
    for d in extracted_journal_dirs:
        if d == "entries":
            entries = True
        if d == "photos":
            photos = True
        if d == "journals":
            journals = True
    if entries and photos and journals:
        return True
    return False


def fix_bullets(entry_text):
    output_text = ""
    for line in entry_text.splitlines():
        if line[:2] == '- ':
            line = '*' + line[3:]
        output_text += '\n'
        output_text += line
    return entry_text


def format_diarly_datetime(dt):
    # like 2020-01-02T15:20:16.594Z
    return dt.strftime("%Y-%m-%dT%H:%M:%S.0000Z")


def markdown_timestamp(dt):
    time_only = dt.strftime("%H:%M")
    return "\n********\n*" + time_only + "*"


def markdown_photo(uuid):
    return "\r\n![](data/" + uuid.lower() + '.jpg)'


def build_objects_from_json(extracted_journal):
    path_to_journals = os.path.join(extracted_journal, 'journals')
    journal_json_files = os.listdir(path_to_journals)
    diarly_journal = {}
    diarly_journal['journals'] = {}
    journal_mapping = {}
    photos_to_move = []
    diarly_entries = {}
    for d in journal_json_files:
        print("processing journal %s in %s" % (d, extracted_journal))
        with open(os.path.join(path_to_journals, d)) as journal_json_file:
            j_json = json.load(journal_json_file)
            j_identifier = j_json['name'].lower()
            j_is_default = False
            if j_json['sortkey'] == 1:
                j_identifier = "defaultJournal"
                j_is_default = True
            diarly_journal['journals'][j_json['name']] = {
                "defaultJournal": j_is_default,
                "noteTemplate": "# Doings\n\n# Learnings\n",
                "identifier": j_identifier
            }
            journal_mapping[j_json['uuid']] = {
                'identifier': j_identifier,
                'name': j_json['name']
            }
            diarly_entries[j_json['name']] = []

    diarly_journal['notes'] = {}
    path_to_entries = os.path.join(extracted_journal, 'entries')
    entries_json_files = os.listdir(path_to_entries)
    for d in entries_json_files:
        print("processing entry %s in %s" % (d, extracted_journal))
        with open(os.path.join(path_to_entries, d)) as entry_json_file:
            e_json = json.load(entry_json_file)
            e_body_markdown = ''
            if 'text' in e_json:
                e_body_markdown = fix_bullets(e_json['text'])
            e_timestamp = datetime.fromtimestamp(e_json['edate'])
            e_modat = datetime.fromtimestamp(e_json['mdate'])
            e_creat = datetime.fromtimestamp(e_json['cdate'])
            e_filename = e_timestamp.strftime("%m-%d") + '.md'
            e_year = e_timestamp.strftime("%Y")
            e_journal_name = journal_mapping[e_json['journalUUID']]['name']
            if 'tags' in e_json:
                e_body_markdown += '\n\n#'
                for t in e_json['tags']:
                    spaceless = "".join(t.split())
                    e_body_markdown += ' #' + spaceless

            e_pictures = ""
            if 'photos' in e_json:
                for p in e_json['photos']:
                    e_pictures += markdown_photo(p)
                    photos_to_move.append({
                        'uuid': p,
                        'journal': e_journal_name,
                        'year': e_year,
                        'timestamp': format_diarly_datetime(e_modat)
                    })

            e_loc_url = ""
            if 'location' in e_json:
                e_loc_long = e_json['location']['longitude']
                e_loc_lat = e_json['location']['latitude']
                e_loc_name = "Location"
                if 'geocode' in e_json['location']:
                    gc = e_json['location']['geocode']
                    if gc is not None:
                        if 'locality' in gc:
                            e_loc_name = gc['locality'] + ', ' + \
                                gc['administrativeArea']

                e_loc_url = '\n\n[' + e_loc_name + '](diarly://map/' + \
                    str(e_loc_lat) + ',' + str(e_loc_long) + ")"

            entry_text = e_pictures + markdown_timestamp(e_timestamp) + \
                '\n\n' + e_body_markdown + e_loc_url

            diarly_entries[e_journal_name].append({
                'text': entry_text,
                'filename': e_filename,
                'year': e_year
            })

            # This can overwrite previous entries since it's just meta
            e_meta_nodename = "/" + e_journal_name + '/' + e_year + \
                '/' + e_filename
            diarly_journal['notes'].update({
                e_meta_nodename: {
                    'lastModifiedAt': format_diarly_datetime(e_modat),
                    'lastModifiedBy': LASTMODIFIEDBY,
                    'charactersCount': 0,
                    'starred': False,
                    'createdAt': format_diarly_datetime(e_creat)
                }
            })

    dy_export_dir = os.path.join(DESTINATION_PATH, 'Export')

    # write notes to file heirarchy
    for jrnl in diarly_entries:
        dy_jrnl_dir = os.path.join(dy_export_dir, jrnl)
        for de in diarly_entries[jrnl]:
            file_path = os.path.join(dy_jrnl_dir, de['year'])
            os.makedirs(file_path, exist_ok=True)
            file_path_name = os.path.join(file_path, de['filename'])
            with open(file_path_name, 'a+') as jrnl_entry:
                jrnl_entry.write(de['text'])
                print('wrote journal entry %s' % file_path_name)

    # move photos to file heirarchy
    diarly_journal['files'] = {}
    path_photos_from = os.path.join(extracted_journal, 'photos')
    for p in photos_to_move:
        wd_name = p['uuid'].upper() + '.jpg'
        d_name = p['uuid'].lower() + '.jpg'
        new_photo_dir = os.path.join(dy_export_dir,
                                     p['journal'],
                                     p['year'],
                                     'data')
        path_old = os.path.join(path_photos_from, wd_name)
        path_new = os.path.join(new_photo_dir, d_name)
        os.makedirs(new_photo_dir, exist_ok=True)
        shutil.move(path_old, path_new)
        diarly_journal['files'].update({
            p['uuid'].lower(): {
                "preferredFilename": "image.jpg",
                "createdAt": p['timestamp']
            }
        })
    leftover_photos = os.listdir(path_photos_from)
    print(leftover_photos)
    print('preserving ' + str(len(leftover_photos)) + ' photos')
    shutil.move(path_photos_from, DESTINATION_PATH)
    # preservs them but not in the zip

    for e in diarly_entries:
        print(e)
    output_diarlymeta_from_objects(diarly_journal)


def entry_json_to_md(wd_entry):
    return "#Title\n\nbody text test"


def output_diarlymeta_from_objects(diarly_journal):
    diarly_journal.update({
        "version": "0.21"
    })
    dy_export_dir = os.path.join(DESTINATION_PATH, 'Export')
    os.makedirs(dy_export_dir, exist_ok=True)
    dy_meta_path = os.path.join(dy_export_dir, 'diarly_meta.json')
    with open(dy_meta_path, 'w') as outfile:
        json.dump(diarly_journal, outfile)
    zip_destination(dy_export_dir)


def clean_intermediate():
    shutil.rmtree(INTERMEDIATE_PATH)
    os.mkdir(INTERMEDIATE_PATH)
    f = open(os.path.join(INTERMEDIATE_PATH, '.gitkeep'), 'x')
    f.write("")
    f.close()
    print('intermediate folder reset')


def zip_destination(path_to_export_dir):
    shutil.make_archive(DESTINATION_PATH_ZIP, 'zip', path_to_export_dir)


source_files = os.listdir(SOURCE_PATH)

directories_to_process = list(map(lambda f: find_extract_zip(f), source_files))

intermediate_dirs = os.listdir(INTERMEDIATE_PATH)

for d in intermediate_dirs:
    if d in (None, "__MACOS", '.gitkeep'):
        print("ignored %s" % d)
    else:
        d_path = os.path.join(INTERMEDIATE_PATH, d)
        if validate_wd(d_path):
            build_objects_from_json(d_path)
        else:
            print("invalid %s" % d)

print('cleanup of ' + str(os.listdir(INTERMEDIATE_PATH)))
print('- ' * 10)

clean_intermediate()

print(os.listdir(INTERMEDIATE_PATH))
print('-' * 20)

# output_diarly_from_objects('test', ('test1, test2'), DESTINATION_PATH)
print("completed migration to %s", DESTINATION_PATH_ZIP)
