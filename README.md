# Take Written Down Export and Make It A Diarly import

## Usage

1. Clone/download this repo or just copy the script and data directories.
1. Open **Written Down** (Version 1.0.6) and go to the gear and then select export as JSON. 
1. Put your exported zip file in the 'data/source' directory. 
1. Run the script with `python3 migrate.py` (if you want to use the sample data, use `python3 migrate.py test`
1. In the `data/destination` directory a new date/time directory is created which contains the migrated data and a zipped copy
1. On your Mac, open **Diarly** (Version 1.3.1) and go to import, select `Markdown (Diarly)`, and import the zip file. 
1. The entries will be imported into the default journal regarless of the active journal or imported journal name

**NOTE** I specify "Mac" above because that's how I tested it

### Prereqs

1. Written Down for iOS version 1.0.6-ish
2. Diarly for MacOS 1.3.1-ish (though the import is versioned so it may be useful over a longer period of time)
3. Python 3.7.4-ish


# Migration Tool Design

I expect the easiest way to do this is to parse all of the JSON files from the Written Down JSON export and convert them into plain old Markdown.  This may be a lossy migration. 

1. extract source into intermediate 
1. validate 
1. build into json objects and markdown strings
1. write output to destination
1. clean up intermediate 
1. zip up resulting export folder


## Destination Directory Structure

* `diarly_meta.json` = file that has metadata about the journal and entries and files
* Journal directory
  * guid-named md files = notes (nothing maps to this from Written down so it can be ignored)
  * Year directory
    * `[MM]-[DD].md` = one file per day which includes all of the entries from the export
    * `data` directory
      * guid-named image files

## Source Directory Structure

* `photos` directory
  * guid-named image files (all jpgs)
* `entries` directory
  * guid-named json files
* `journals` directory 
  * guid-named json files


## Entry Transformation

For each entry: 

1. add the note meta information to the `diarly_meta.json` file. 
1. use the `edate` node to get the month and day for `[MM]-[DD].md` filename (either create if none exists or start a new one if one does exist)
1. use the time portion of edate to make a timestamp to load if there are multiple entries for a day `********\n*15:22*\n`
1. add the `text` node from the source to the `[MM]-[DD].md` file for the day.  Start each one with the above timestamp. 
1. add each child node of the `tags` node with # prepended to the strings. 
1. add each child node of the `photos` node based on guid to the `[MM]-[DD].md` file with the markdown link format: 
`![](data/05f0d1a3-285c-4c8b-b1f0-7d814a934b7d.jpg)`
1. copy the file from `source/pictures` to `[YYYY]/data` directory.
1. add the `location` node from the source to the `[MM]-[DD].md` file with the markdown link format: `[Mouthcard](diarly://map/37.38354876316343,-82.25316213149385)`
1. if geocode exists under location, pull `locality`, `administrativeArea` for the name part in `[]` since it represents city and state
1. Set the last modified by to a device ID associated with your account using envrionment variable `LASTMODIFIEDBY`

In order to get a `lastModifiedBy` value, I pulled it from the markdown export from Diarly